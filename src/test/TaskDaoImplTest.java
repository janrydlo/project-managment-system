package cz.uhk.ppro.pms.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.uhk.ppro.pms.dao.ITaskDao;
import cz.uhk.ppro.pms.entities.Task;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "test-context.xml"})
public class TaskDaoImplTest {

	@Autowired
	ITaskDao taskDao;
	
	@Test
	public void testGetTasksForProject() {
		List<Task> tasks = taskDao.getTasksForProject(1);
		assertTrue(tasks.size() > 0);
		
		tasks = taskDao.getTasksForProject(0);
		assertTrue(tasks.isEmpty());
	}

	@Test
	public void testGetTaskNames() {
		List<String> tasks  = taskDao.getTaskNames("t");
		assertTrue(tasks.size() > 0);
		
		tasks  = taskDao.getTaskNames(null);
		assertTrue(tasks.size() > 0);
		
		tasks = taskDao.getTaskNames("f");
		assertTrue(tasks.isEmpty());
	}

	@Test
	public void testGetTaskByName() {
		List<Task> tasks = taskDao.getTaskByName("t");
		assertTrue(tasks.size() > 0);
		
		tasks  = taskDao.getTaskByName(null);
		assertTrue(tasks.size() > 0);
		
		tasks = taskDao.getTaskByName("f");
		assertTrue(tasks.isEmpty());
	}


	@Test
	public void testGetTasksByUser() {
		List<Task> tasks = taskDao.getTasksByUser(1);
		assertTrue(tasks.size() > 0);
		
		tasks = taskDao.getTasksByUser(0);
		assertTrue(tasks.isEmpty());
	}

}
