package cz.uhk.ppro.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import cz.uhk.ppro.pms.entities.Comment;
import cz.uhk.ppro.pms.service.ICommentService;

@Controller
@SessionAttributes({"priorityList", "userList", "typeList", "statusList"})
public class CommentController {

	 @Autowired(required = true)
	 private ICommentService commmentService;

	@RequestMapping(value = "/comments/task/{id}", method = RequestMethod.GET )
    public String listCommentsForTask(Model model, @PathVariable("id") int id){
        model.addAttribute("comment", new Comment());
        model.addAttribute("listComments", commmentService.findAllForTask(id));
        return "comments";
    }
	
	  @RequestMapping("/comments/remove/{id}")
	    public String deleteTask(@PathVariable("id") int id){
	       commmentService.delete(commmentService.read(id));
	        return "redirect:/task/detail/{id}";
	    }

	
	
}
