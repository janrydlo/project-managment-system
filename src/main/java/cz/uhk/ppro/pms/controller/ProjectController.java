package cz.uhk.ppro.pms.controller;

import java.util.List;

import cz.uhk.ppro.pms.entities.*;
import cz.uhk.ppro.pms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import cz.uhk.ppro.pms.validator.ProjectValidator;

@Controller
@SessionAttributes({"priorityList", "userList", "typeList", "statusList"})
public class ProjectController {

	@Autowired(required = true)
	private ITaskService taskService;
	@Autowired(required = true)
	private IPriorityService priorityService;
	@Autowired(required = true)
	private ITypeService typeService;
	@Autowired(required = true)
	private IProjectService projectService;
	@Autowired(required = true)
	private IUserService userService;
	@Autowired(required = true)
	private IStatusService statusService;
	@Autowired
	private ICommentService commentService;

	@Autowired
    private ProjectValidator projectValidator;

    @RequestMapping(value = "/projects", method = RequestMethod.GET )
    public String listProjects(Model model){
        model.addAttribute("project", new Project());
        model.addAttribute("listProjects", projectService.getAll());    	
        return "projects";
    }
    
    @RequestMapping(value = "/projects", method = RequestMethod.POST )
    public String listProjectsFiltr(Model model, @RequestParam("nameFiltr") String nameFilter){
        model.addAttribute("project", new Project());
        model.addAttribute("listProjects", projectService.getProjectsByName(nameFilter));
        return "projects";
    }

    @RequestMapping(value = "/projects/getNames", method = RequestMethod.GET)
	public @ResponseBody
	List<String> getNames(@RequestParam String name) {

		return projectService.getProjectNames(name);

	}

    @RequestMapping(value = "/manage/project/create", method = RequestMethod.GET)
    public String createProject(Model model) {    	
        model.addAttribute("priorityList", priorityService.getAll());
    	model.addAttribute("typeList", typeService.getAll());
    	model.addAttribute("userList", userService.findAllManagersAndAdmins());
    
        model.addAttribute("projectForm", new Project());
        return "createProject";
    }

    @RequestMapping(value = "/manage/project/create", method = RequestMethod.POST)
    public String createProject(@ModelAttribute("projectForm") Project projectForm, BindingResult bindingResult) {
    	projectValidator.validate(projectForm, bindingResult);    	

	   if (bindingResult.hasErrors()) {
           return "createProject";
       }
		   
        projectService.create(projectForm);
        return "redirect:/projects";
    }

    @RequestMapping("/manage/project/remove/{id}")
    public String deleteProject(@PathVariable("id") int id){
		for(Task task: projectService.read(id).getTasks()) {
			for(Comment comment : task.getComments()) {
				commentService.delete(comment);
			}
			taskService.delete(task);
		}
        projectService.delete(projectService.read(id));
        return "redirect:/projects";
    }

    @RequestMapping(value ="/manage/project/edit/{id}", method=RequestMethod.GET)
    public String editProject(@PathVariable("id") int id, Model model){
     	model.addAttribute("priorityList", priorityService.getAll());
    	model.addAttribute("typeList", typeService.getAll());
    	model.addAttribute("projectList", projectService.findAllActiveProjects());
    	model.addAttribute("userList", userService.findAllManagersAndAdmins());
    	model.addAttribute("statusList",statusService.getAll());
        model.addAttribute("projectForm", projectService.read(id));
        model.addAttribute("listProjects", projectService.getAll());

        return "editProject";
    }
    
    @RequestMapping(value = "/manage/project/edit/{id}", method = RequestMethod.POST )
    public String saveProject(@ModelAttribute("projectForm") Project projectForm, BindingResult bindingResult) {
    	projectValidator.validate(projectForm, bindingResult);    	

	 	   if (bindingResult.hasErrors()) {
	            return "editProject";
	        }
    	      
        projectService.update(projectForm);
        return "redirect:/projects";
    }

	@RequestMapping(value = "/project/detail/{id}", method = RequestMethod.GET)
	public String detailProject(@PathVariable("id") int id, Model model){
		model.addAttribute("projectForm", projectService.read(id));
		return "detailProject";
	}

}
