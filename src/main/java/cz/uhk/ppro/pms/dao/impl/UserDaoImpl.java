package cz.uhk.ppro.pms.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.IUserDao;
import cz.uhk.ppro.pms.entities.User;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements IUserDao {

	@Override
	public User findByUsername(String username) {
		Criteria c = currentSession().createCriteria(User.class);
		c.add(Restrictions.eq("username", username));
		return (User)c.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllManagersAndAdmins() {
		List<String> names = new ArrayList<String>();
		names.add("ROLE_ADMIN");
		names.add("ROLE_MANAGER");
		
		Criteria c = currentSession().createCriteria(User.class);
//		c.add(Restrictions.in("roles.name", names));
		return c.list();
	}
}
