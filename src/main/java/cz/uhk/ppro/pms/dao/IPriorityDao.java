package cz.uhk.ppro.pms.dao;

import cz.uhk.ppro.pms.entities.Priority;

public interface IPriorityDao extends IGenericDao<Priority, Integer>{
	
}
