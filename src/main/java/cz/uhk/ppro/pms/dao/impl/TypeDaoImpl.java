package cz.uhk.ppro.pms.dao.impl;

import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.ITypeDao;
import cz.uhk.ppro.pms.entities.Type;

@Repository
public class TypeDaoImpl extends GenericDaoImpl<Type, Integer> implements ITypeDao {
}
