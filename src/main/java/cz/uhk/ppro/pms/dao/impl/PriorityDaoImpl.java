package cz.uhk.ppro.pms.dao.impl;

import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.IPriorityDao;
import cz.uhk.ppro.pms.entities.Priority;

@Repository
public class PriorityDaoImpl extends GenericDaoImpl<Priority, Integer> implements IPriorityDao {
}
