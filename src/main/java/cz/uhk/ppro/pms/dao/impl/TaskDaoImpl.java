package cz.uhk.ppro.pms.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.ITaskDao;
import cz.uhk.ppro.pms.entities.Task;

@Repository
public class TaskDaoImpl extends GenericDaoImpl<Task, Integer> implements ITaskDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> getTasksForProject(int idProject) {
		Criteria c = currentSession().createCriteria(Task.class);
		c.add(Restrictions.eq("project.idProject", idProject));
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTaskNames(String name) {
		Criteria c = currentSession().createCriteria(Task.class);
		if(name != null) {
			c.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		}
		c.setProjection(Projections.property("name"));
		
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> getTaskByName(String name) {
		Criteria c = currentSession().createCriteria(Task.class);
		if(name != null) {
			c.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		}
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> getTasksByUser(int idUser) {
		Criteria c = currentSession().createCriteria(Task.class);
		c.add(Restrictions.eq("user.idUser", idUser));
		return c.list();
	}

	@Override
	public List<Task> getActiveTasksByUser(int idUser, int idStatus) {
		Criteria c = currentSession().createCriteria(Task.class);
		c.add(Restrictions.eq("user.idUser", idUser));
		c.add(Restrictions.ne("status.idStatus", idStatus));
		return c.list();
	}
}
