package cz.uhk.ppro.pms.dao;

import java.util.List;

import cz.uhk.ppro.pms.entities.User;

public interface IUserDao extends IGenericDao<User, Integer>{
	
	/**
	 * Method finds user by his username
	 * @param username 
	 * @return returns {@link User} object
	 */
	public User findByUsername(String username);
	
	/**
	 * Method finds all users with role Admin and Manager
	 * @return returns collection of {@link User} objects
	 */
	public List<User> findAllManagersAndAdmins();
}
