package cz.uhk.ppro.pms.dao.impl;

import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.IRoleDao;
import cz.uhk.ppro.pms.entities.Role;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, Integer> implements IRoleDao {
}
