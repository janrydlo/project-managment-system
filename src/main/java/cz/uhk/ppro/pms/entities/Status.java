package cz.uhk.ppro.pms.entities;


// Generated 30.1.2017 19:40:29 by Hibernate Tools 4.3.5.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Status generated by hbm2java
 */
@Entity
@Table(name = "Status", catalog = "pms", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Status implements java.io.Serializable {

	private Integer idStatus;
	private String name;
	private Set<Task> tasks = new HashSet<Task>(0);
	private Set<Project> projects = new HashSet<Project>(0);

	public Status() {
	}

	public Status(String name) {
		this.name = name;
	}

	public Status(String name, Set<Task> tasks, Set<Project> projects) {
		this.name = name;
		this.tasks = tasks;
		this.projects = projects;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "idStatus", unique = true, nullable = false)
	public Integer getIdStatus() {
		return this.idStatus;
	}

	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	@Column(name = "name", unique = true, nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

}
