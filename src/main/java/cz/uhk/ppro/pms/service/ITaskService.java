package cz.uhk.ppro.pms.service;

import java.util.List;

import cz.uhk.ppro.pms.entities.Task;

public interface ITaskService extends IGenericService<Task, Integer> {
	/**
	 * Method finds all tasks related to project
	 * @param idProject unique identifier of project
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getAllForProject(int idProject);
	
	/**
	 * Method finds all task names by parama
	 * @param name partial or whole name
	 * @return collection of string
	 */
	public List<String> getTaskNames(String name);
	
	/**
	 * Method finds all task with corresponding name
	 * @param name whole or partial name
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getTasksByName(String name);
	
	/**
	 * Method finds all tasks with corresponding user
	 * @param idUser unique identifier of User
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getTasksByUser(int idUser);

	/**
	 * Method finds all tasks with corresponding user
	 * @param idUser unique identifier of User
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getActiveTasksByUser(int idUser, int idStatus);
}
