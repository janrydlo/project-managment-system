package cz.uhk.ppro.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.entities.Role;
import cz.uhk.ppro.pms.service.IRoleService;

@Service
public class RoleServiceImpl extends GenericServiceImpl<Role, Integer> implements IRoleService {

    @Autowired
    public RoleServiceImpl(@Qualifier("roleDaoImpl") IGenericDao<Role, Integer> genericDao) {
        super(genericDao);
    }
}
