package cz.uhk.ppro.pms.service.impl;

import java.util.List;

import cz.uhk.ppro.pms.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.dao.IUserDao;
import cz.uhk.ppro.pms.entities.User;
import cz.uhk.ppro.pms.service.IUserService;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, Integer> implements IUserService {

	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	 @Autowired
	 private IUserDao userDao;
	 
	 @Autowired
	 private IRoleService roleService;
	
	@Autowired
    public UserServiceImpl(@Qualifier("userDaoImpl") IGenericDao<User, Integer> genericDao) {
    	super(genericDao);
    }

	@Override
	public Integer create(User user) {
		 user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
	     user.setRole(user.getRole());
	     return userDao.create(user);
	}

	@Override
	public void update(User user) {
		user.setName(user.getName());
		user.setSurname(user.getSurname());
		user.setRole(user.getRole());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userDao.update(user);
	}

	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public List<User> findAllManagersAndAdmins() {
		return userDao.findAllManagersAndAdmins();
	}
    
}
