package cz.uhk.ppro.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.entities.Type;
import cz.uhk.ppro.pms.service.ITypeService;

@Service
public class TypeServiceImpl extends GenericServiceImpl<Type, Integer> implements ITypeService {

	
	@Autowired
    public TypeServiceImpl(@Qualifier("typeDaoImpl") IGenericDao<Type, Integer> genericDao) {
    	super(genericDao);
    }
    
}
