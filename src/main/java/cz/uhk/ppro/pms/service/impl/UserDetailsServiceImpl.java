package cz.uhk.ppro.pms.service.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.pms.dao.IUserDao;
import cz.uhk.ppro.pms.entities.User;

public class UserDetailsServiceImpl implements UserDetailsService{
	
	 @Autowired
	 private IUserDao userDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User u = userDao.findByUsername(username);
		if(u == null)  {
			throw new UsernameNotFoundException("Uzivatel se nenalezl");
		}
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(u.getRole().getRoleName());

		return new org.springframework.security.core.userdetails.User(u.getUsername(), u.getPassword(), Collections.singleton(grantedAuthority));
    }
	
}
