package cz.uhk.ppro.pms.service.impl;

import java.util.Date;
import java.util.List;

import cz.uhk.ppro.pms.dao.*;
import cz.uhk.ppro.pms.entities.*;
import cz.uhk.ppro.pms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ProjectServiceImpl extends GenericServiceImpl<Project, Integer> implements IProjectService {

	 @Autowired
	 private IProjectDao projectDao;

	 @Autowired
	 private IStatusService statusService;

	 @Autowired
	 private IPriorityService priorityService;

	 @Autowired
	 private ITypeService typeService;

	 @Autowired
	 private IUserService userService;

	@Autowired
    public ProjectServiceImpl(@Qualifier("projectDaoImpl") IGenericDao<Project, Integer> genericDao) {
    	super(genericDao);
    }

	@Override
	public Integer create(Project project) {
		project.setUser(userService.read(project.getUser().getIdUser()));
		project.setStartDate(new Date());
		project.setStatus(statusService.read(1));

		return projectDao.create(project);
	}

	@Override
	public void update(Project project) {
		Status st = statusService.read(project.getStatus().getIdStatus());

		project.setPriority(priorityService.read(project.getPriority().getIdPriority()));
		project.setUser(userService.read(project.getUser().getIdUser()));
		project.setType(typeService.read(project.getType().getIdType()));
		project.setStatus(st);
		if(st.getName().equals("Vyřešený") || st.getName().equals("Uzavřený")) {
			project.setEndDate(new Date());
		}
		projectDao.update(project);
	}

	@Override
	public List<Project> findAllActiveProjects() {
		return projectDao.findAllActiveProjects();
	}

	@Override
	public List<String> getProjectNames(String name) {
		return projectDao.findProjectNames(name);
	}

	@Override
	public List<Project> getProjectsByName(String name) {
		return projectDao.findProjectByName(name);
	}

	@Override
	public List<Project> getProjectsByUser(int idUser) {
		return projectDao.findProjectByUser(idUser);
	}
    
}
