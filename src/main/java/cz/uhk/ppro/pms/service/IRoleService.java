package cz.uhk.ppro.pms.service;

import cz.uhk.ppro.pms.entities.Role;

public interface IRoleService extends IGenericService<Role, Integer> {
}
