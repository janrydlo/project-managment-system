package cz.uhk.ppro.pms.service;

import java.util.List;

import cz.uhk.ppro.pms.entities.Project;

public interface IProjectService extends IGenericService<Project, Integer> {
	/**
	 * Method which finds all projects with other status than complete
	 * @return list of found Projects
	 */
	public List<Project> findAllActiveProjects();
	
	/**
	 * Method finds names of projects with corresponding name
	 * @param name whole or partial name of project
	 * @return returns collection of String
	 */
	public List<String> getProjectNames(String name);
	
	/**
	 * Method finds projects by name
	 * @param name partial or whole name
	 * @return returns collection of {@link Project}
	 */
	public List<Project> getProjectsByName(String name);
	
	/**
	 * Method finds all project for user with corresponding identifier
	 * @param idUser unique identifier of User
	 * @return returns collection of {@link Project}
	 */
	public List<Project> getProjectsByUser(int idUser);
}
