package cz.uhk.ppro.pms.service;

import cz.uhk.ppro.pms.entities.Status;

public interface IStatusService extends IGenericService<Status, Integer> {

}
