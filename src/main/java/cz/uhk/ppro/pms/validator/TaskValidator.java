package cz.uhk.ppro.pms.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cz.uhk.ppro.pms.entities.Task;

public class TaskValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Task.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    	System.out.print(errors);
        Task task = (Task) o;
        

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        if (task.getName().length() < 6 || task.getName().length() > 200) {
        	System.out.println("1");
            errors.rejectValue("name", "Size.taskForm.name");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "NotEmpty");
        if (task.getName().length() < 6 || task.getName().length() > 500) {
        	System.out.println("2");
            errors.rejectValue("description", "Size.taskForm.description");
        }
        
       if(task.getType().getIdType() == 0) {
    	   System.out.println("3");
    	   errors.rejectValue("type.idType", "NotEmpty");
       }
       if(task.getPriority().getIdPriority() == 0) {
    	   System.out.println("4");
    	   errors.rejectValue("priority.idPriority", "NotEmpty");
       }
       if(task.getUser().getIdUser() == 0) {
    	   System.out.println("5");
    	   errors.rejectValue("user.idUser", "NotEmpty");
       }
       if(task.getProject().getIdProject() == 0) {
    	   System.out.println("6");
    	   errors.rejectValue("project.idProject", "NotEmpty");
       }
       if(task.getEstimatedTime() == 0) {
    	   System.out.println("7");
    	   errors.rejectValue("estimatedTime", "Size.taskForm.estimatedTime");
       }
   
    }
}
