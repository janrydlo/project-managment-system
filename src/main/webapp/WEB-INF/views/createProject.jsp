<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
<form:form method="POST" modelAttribute="projectForm" class="form-signin">    
    <h2 class="form-signin-heading">Vytvoření projektu</h2>

    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="name" class="control-label">Název projektu</label>
        <form:input type="text" path="name" class="form-control" placeholder="Název projektu" autofocus="true"></form:input>
        <form:errors path="name"></form:errors>
    </div>
    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="description" class="control-label">Popis projektu</label>
       	<form:textarea path="description" rows="8" cols="30" id="description"/>
        <form:errors path="description"></form:errors>
    </div>
     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="user_id" class="control-label">Vedoucí projektu</label>
        <form:select class="form-control" id="user_id" path="user.idUser">
        	<form:option value="0" label=""></form:option>
        	<c:forEach items="${userList}" var="user">
                <c:if test="${user.role.roleName != 'ROLE_USER'}">
                    <form:option value="${user.idUser}" label="${user.name} ${user.surname}"></form:option>
                </c:if>
        	</c:forEach>
        </form:select>
         <form:errors path="user.idUser"></form:errors>
    </div>

    <div class="form-group col-md-8">
        <button class="btn btn-primary" type="submit">Vytvořit projekt</button>
    </div>
</form:form>