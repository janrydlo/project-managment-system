<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
<h1>Seznam Komentářů</h1>
<c:if test="${!empty listComments}">
    <table class="table table-striped">
        <tr>
            <th width="30">ID</th>
            <th width="80">Uživatel</th>
            <th width="60">Nadpis</th>
            <th width="60">Obsah</th>
            <th width="60"></th>
            <th width="60"></th>
        </tr>
        <c:forEach items="${listComments}" var="comment">
            <tr>
                <td>${comment.idComment}</td>
                <td>${comment.user.name} ${comment.user.surname}</td>
                <td>${comment.title}</td>
                <td>${comment.content}</td>
                <td><a href="<c:url value='/task/edit/${task.idTask}' />" >Upravit</a></td>
                <td><a href="<c:url value='/task/remove/${task.idTask}' />" >Smazat</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<a class="btn btn-default" href="/task/create" role="button">Vytvořit úkol</a>