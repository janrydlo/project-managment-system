<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
    <form>

        <div class="form-group">
            <label for="user_firstname" class="control-label">Jméno</label>
            <input type="text" class="form-control" id="user_firstname" name="user_firstname" placeholder="Jméno...">
        </div>

        <div class="form-group">
            <label for="user_lastname" class="control-label">Příjmení</label>
            <input type="text" class="form-control" id="user_lastname" name="user_lastname" placeholder="Příjmení...">
        </div>

        <div class="form-group">
            <label for="username" class="control-label">Uživatelské jméno</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Uživatelské jméno...">
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Heslo</label>
            <input type="text" class="form-control" id="password" name="password" placeholder="Heslo...">
        </div>

        <div class="form-group">
            <label for="role_id" class="control-label">Role</label>
            <select class="form-control" id="role_id">
                <option value="1">Uživatel</option>
                <option value="2">Vedoucí</option>
                <option value="3">Administrátor</option>
            </select>
        </div>

    </form>