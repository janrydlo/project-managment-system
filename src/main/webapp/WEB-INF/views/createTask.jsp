<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
 <form:form method="POST" modelAttribute="taskForm">
 <h2>Vytvoření úkolu</h2>
     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="task_name" class="control-label">Název úkolu</label>
        <form:input type="text" class="form-control" id="task_name" path="name" placeholder="Název úkolu"></form:input>
        <form:errors path="name"></form:errors>
    </div>

     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="task_description" class="control-label">Popis úkolu</label>
       	<form:textarea path="description" rows="8" cols="30" id="task_description"/>
        <form:errors path="description"></form:errors>
    </div>

     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="user_id" class="control-label">Řešitel úkolu</label>
        <form:select id="user_id" class="form-control" path="user.idUser">
        <form:option value="0" label=""></form:option>
        	<c:forEach items="${userList}" var="user">
                <form:option value="${user.idUser}" label="${user.name} ${user.surname}"></form:option>
        	</c:forEach>
        </form:select>
        <form:errors path="user.idUser"></form:errors>
    </div>
    
    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="project_id" class="control-label">Projekt</label>
        <form:select id="project_id" class="form-control" path="project.idProject">
        <form:option value="0" label=""></form:option>
        	<c:forEach items="${projectList}" var="project">
        		<form:option value="${project.idProject}" label="${project.name}"></form:option>
        	</c:forEach>
        </form:select>
        <form:errors path="project.idProject"></form:errors>
    </div>


    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="type_id" class="control-label">Typ</label>
        <form:select class="form-control" id="type_id" path="type.idType" >
        <form:option value="0" label=""></form:option>
        	<c:forEach items="${typeList}" var="type">
        		<form:option value="${type.idType}" label="${type.name}"></form:option>
        	</c:forEach>
        </form:select>
        <form:errors path="type.idType"></form:errors>
    </div>
    
     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="priority_id" class="control-label">Priorita</label>
        <form:select class="form-control" id="priority_id" path="priority.idPriority" >
        <form:option value="0" label=""></form:option>
        	<c:forEach items="${priorityList}" var="priority">
        		<form:option value="${priority.idPriority}" label="${priority.name}"></form:option>
        	</c:forEach>
        </form:select>
        <form:errors path="priority.idPriority"></form:errors>
    </div>

    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="estimated_time" class="control-label">Odhadovaný čas</label>
        <form:input type="text" class="form-control" id="estimated_time" path="estimatedTime" placeholder="24h"></form:input>
        <form:errors path="estimatedTime"></form:errors>
    </div>

     <div class="form-group col-md-8">
        <button type="submit" class="btn btn-primary">Vytvořit úkol</button>
    </div>

</form:form>