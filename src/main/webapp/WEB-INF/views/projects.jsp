<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
<h2>Seznam projektů</h2>

<form:form>
  <div class="row">
    <div class="col-lg-12">
      <label>Název projektu: </label>
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group">
            <input id="w-input-search" class="form-control" name="nameFiltr" type="text">
          </div>
        </div>
        <div class="col-xs-2">
          <div class="form-group">
           <button type="submit" class="btn btn-primary">Vyhledat</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form:form>
<c:if test="${!empty listProjects}">
    <table class="table table-striped">
        <tr>
            <th width="80">Jméno projektu</th>
            <th width="60">Vedoucí projektu</th>
            <th width="60">Datum vytvoření</th>
            <th width="60">Datum ukončení</th>
            <th width="60">Status projektu</th>
            <th width="20">Úkoly</th>
            <security:authorize access="hasAnyRole('ROLE_MANAGER','ROLE_ADMIN')">
                <th width="40"></th>
                <th width="40"></th>
            </security:authorize>
        </tr>
        <c:forEach items="${listProjects}" var="project">
            <tr>
                <td><a href="<c:url value='/project/detail/${project.idProject}' />" >${project.name}</a></td>
                <td>${project.user.name} ${project.user.surname}</td>
                <td><fmt:formatDate value="${project.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                <td><fmt:formatDate value="${project.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                <td>${project.status.name}</td>
                <td><a href="<c:url value='/tasks/project/${project.idProject}' />" >${fn:length(project.tasks)}</a></td>
                <security:authorize access="hasAnyRole('ROLE_MANAGER','ROLE_ADMIN')">
                    <td><a class="btn btn-info" href="<c:url value='/manage/project/edit/${project.idProject}' />" role="button">Upravit</a></td>
                    <td><a class="btn btn-danger" href="<c:url value='/manage/project/remove/${project.idProject}' />" role="button">Smazat</a></td>
                </security:authorize>
            </tr>
        </c:forEach>
    </table>
</c:if>
<security:authorize access="hasAnyRole('ROLE_MANAGER','ROLE_ADMIN')">
    <a class="btn btn-primary" href="/manage/project/create" role="button">Vytvořit projekt</a>
</security:authorize>

<script>
 document.body.onload = function(){
	$('#w-input-search').autocomplete({
		 source: function (request, response) {
		        $.getJSON("/projects/getNames?name=" + request.term, function (data) {
		        	console.log(data);
		            response($.map(data, function (value, key) {
		            	console.log(value);
		            	console.log(key);
		                return {
		                    label: value,
		                    value: key
		                };
		            }));
		        });
		    },
		    minLength: 2,
		    delay: 100
		});
 }
  </script>