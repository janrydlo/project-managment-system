<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>

      <div class="container">
  		<h2>Detail tasku</h2>       
		  <table class="table table-striped">
		      <tr>
		        <th>Název</th>
		        <td>${taskForm.name}</td>
		      </tr>
		       <tr>
		        <th>Projekt</th>
		        <td><a href="<c:url value='/project/detail/${taskForm.project.idProject}'/>">${taskForm.project.name}</a></td>
		      </tr>
		       <tr>
		        <th>Uživatel</th>
		        <td> ${taskForm.user.name} ${taskForm.user.surname}</td>
		      </tr>
		      <tr>
		        <th>Popis</th>
		        <td>${taskForm.description}</td>
		      </tr>
		      <tr>
		        <th>Priorita</th>
		        <td>${taskForm.priority.name}</td>
		      </tr>
		       <tr>
		        <th>Status</th>
		        <td>${taskForm.status.name}</td>
		      </tr>
		        <tr>
		        <th>Type</th>
		        <td>${taskForm.type.name}</td>
		      </tr>
		      <tr>
		        <th>Datum počátku</th>
		       	<td><fmt:formatDate value="${taskForm.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
		      </tr>
		      <tr>
		        <th>Datum konec</th>
		       <td><fmt:formatDate value="${taskForm.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>
		      </tr>
		  </table>
		</div>
		<br/>
		
		<div class="well">
                    <h4>Napsat komentář k úkolu</h4>
                     <form:form method="POST" modelAttribute="commentForm" class="form-signin" >
                           <form:textarea path="content" rows="5" cols="30" id="task_description"/>
                            <input
                type="hidden" name="task.idTask" value="${taskForm.idTask}" />
                        <button type="submit" value="" class="btn btn-primary"> Submit</button>
                       
                    </form:form>
                </div>
		<br/>
		<c:if test="${not empty taskForm.comments}">
		<h2>Komentáře</h2>     
		<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<div class="container">
	<c:forEach items="${taskForm.comments}" var="comment">
    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-white post panel-shadow">
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <b>${comment.user.username}</b> ${comment.title}
                        </div>
                        <a href="<c:url value='/comments/remove/${comment.idComment}' />" >Smazat</a>
                    </div>
                </div> 
                <div class="post-description"> 
                    <p>${comment.content}</p>
                </div>
            </div>
        </div>
        </div>
        </c:forEach>
</div>
		</c:if>
		
</html>