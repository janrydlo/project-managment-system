<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>

    <div class="container col-md-12">
  		<h1>Uživatelský dashboard</h1>
        <div class="container col-md-4">
            <h2>Detail uživatele</h2>
		  <table class="table table-striped">
		      <tr>
		        <th>Uživatelské jméno</th>
		        <td>${user.username}</td>
		      </tr>
		       <tr>
		        <th>Jméno</th>
		        <td><c:out value="${user.name}"></c:out> </td>
		      </tr>
		       <tr>
		        <th>Příjmení</th>
		         <td><c:out value="${user.surname}"></c:out> </td>
		      </tr>
		      <tr>
		        <th>Role v systému</th>
		        <td><c:out value="${user.role}"></c:out></td>
		      </tr>
		  </table>
        </div>
		<br/>

    <div class="container col-md-8">
        <c:if test="${not empty listProjects}">
		<h2>Spravované projekty</h2>     
		<table class="table table-striped">
        <tr>
            <th width="80">Jméno projektu</th>
            <th width="60">Datum vytvoření</th>
            <th width="60">Datum ukončení</th>
            <th width="60">Status projektu</th>
            <c:forEach items="${listProjects}" var="project">
                <tr>
                    <td><a href="<c:url value='/project/detail/${project.idProject}' />" ><c:out value="${project.name}"></c:out></a></td>
                    <td><fmt:formatDate value="${project.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                    <td><fmt:formatDate value="${project.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                    <td><c:out value="${project.status.name}"></c:out></td>
                 </tr>
            </c:forEach>
		</table>
        </c:if>
        <br/>
        <h2>Přidělené úkoly</h2>
		<table class="table table-striped col-md-5">
        <tr>
            <th width="120">Název úkolu</th>
            <th width="60">Priorita úkolu</th>
            <th width="60">Status úkolu</th>
            <th width="60">Odhadovaná doba</th>
            <th width="60">Datum vytvoření</th>
            <%--<th width="60">Datum ukončení</th>--%>
        </tr>
            <c:choose>
                <c:when test="${not empty listTasks}">
                <c:forEach items="${listTasks}" var="task">
                    <%--<c:if test="${task.status.name != 'Uzavřený'}">--%>
                        <tr>
                            <td><a href="<c:url value='/project/detail/${task.project.idProject}' />" >${task.project.name}</a> - <a href="<c:url value='/task/detail/${task.idTask}' />" >${task.name}</a></td>
                            <td><c:out value="${task.priority.name}"></c:out></td>
                            <td><c:out value="${task.status.name}"></c:out></td>
                            <td><c:out value="${task.estimatedTime}">h</c:out></td>
                            <td><fmt:formatDate value="${task.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                            <%--<td><fmt:formatDate value="${task.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>--%>
                        </tr>
                    <%--</c:if>--%>
                </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td colspan="6" style="text-align: center">Nemáte žádné úkoly.</td>
                    </tr>
                </c:otherwise>
            </c:choose>
   		 </table>
    </div>
</div>