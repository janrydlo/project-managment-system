<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>

<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/dashboard">Project Management System</a>
</div>
<div id="navbar" class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
        <li class="active"><a href="/dashboard">Přehled</a></li>
        <li><a href="/projects">Projekty</a></li>
        <li><a href="/tasks">Úkoly</a></li>
        <li><a href="/users">Uživatelé</a></li>
         <li><a href="/dashboard">User: ${pageContext.request.userPrincipal.name}</a> </li>
         <li><a href="/logout">Odhlásit</a></li>
    </ul>
</div>